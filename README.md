# Speech processing experiments

This is a collection of small interactive experiments to demonstrate effects of speech signals and in speech processing. 

- [Harmonic signals](Harmonic_signals.ipynb)
- [Sampling and quantization](Sampling_and_quantization.ipynb)


